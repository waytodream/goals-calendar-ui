/* tslint:disable */
export interface SingUpDto {

  /**
   * User login
   */
  login: string;

  /**
   * User name
   */
  name: string;

  /**
   * User password
   */
  password: string;
}
