/* tslint:disable */
import { UserDto } from './user-dto';
export interface UsersResponse {

  /**
   * Code of operation result
   */
  code: 200 | 400 | 500 | 0;

  /**
   * message will be filled in some causes
   */
  message?: string;
  data: Array<UserDto>;
}
