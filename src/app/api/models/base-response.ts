/* tslint:disable */
export interface BaseResponse {

  /**
   * Code of operation result
   */
  code: 200 | 400 | 500 | 0;

  /**
   * message will be filled in some causes
   */
  message?: string;

  /**
   * Date of operation result
   */
  data?: {};
}
