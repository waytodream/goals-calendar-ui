/* tslint:disable */
export interface SingInDto {

  /**
   * User login
   */
  login: string;

  /**
   * User password
   */
  password: string;
}
