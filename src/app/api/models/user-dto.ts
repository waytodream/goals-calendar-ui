/* tslint:disable */
export interface UserDto {

  /**
   * User ID
   */
  id: number;

  /**
   * User name
   */
  name: string;

  /**
   * User login
   */
  login: string;
}
