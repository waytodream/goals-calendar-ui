/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { UsersResponse } from '../models/users-response';
import { UserResponse } from '../models/user-response';
import { UserDto } from '../models/user-dto';
@Injectable({
  providedIn: 'root',
})
class UserService extends __BaseService {
  static readonly getUsersPath = '/users/';
  static readonly postUsersPath = '/users/';
  static readonly getUsersUserIdPath = '/users/{userId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Returns list of all users
   * @return Array of users
   */
  getUsersResponse(): __Observable<__StrictHttpResponse<UsersResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/users/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UsersResponse>;
      })
    );
  }
  /**
   * Returns list of all users
   * @return Array of users
   */
  getUsers(): __Observable<UsersResponse> {
    return this.getUsersResponse().pipe(
      __map(_r => _r.body as UsersResponse)
    );
  }

  /**
   * Returns created user
   * @param body Object for transfer user
   * @return Object of user
   */
  postUsersResponse(body?: UserDto): __Observable<__StrictHttpResponse<UserResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/users/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserResponse>;
      })
    );
  }
  /**
   * Returns created user
   * @param body Object for transfer user
   * @return Object of user
   */
  postUsers(body?: UserDto): __Observable<UserResponse> {
    return this.postUsersResponse(body).pipe(
      __map(_r => _r.body as UserResponse)
    );
  }

  /**
   * Returns object of user
   * @param userId userId
   * @return Object of user
   */
  getUsersUserIdResponse(userId: string): __Observable<__StrictHttpResponse<UserResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/users/${userId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserResponse>;
      })
    );
  }
  /**
   * Returns object of user
   * @param userId userId
   * @return Object of user
   */
  getUsersUserId(userId: string): __Observable<UserResponse> {
    return this.getUsersUserIdResponse(userId).pipe(
      __map(_r => _r.body as UserResponse)
    );
  }
}

module UserService {
}

export { UserService }
