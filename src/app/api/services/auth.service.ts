/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { SignUpResponse } from '../models/sign-up-response';
import { SingUpDto } from '../models/sing-up-dto';
import { SignInResponse } from '../models/sign-in-response';
import { SingInDto } from '../models/sing-in-dto';
@Injectable({
  providedIn: 'root',
})
class AuthService extends __BaseService {
  static readonly postAuthSignUpPath = '/auth/signUp';
  static readonly postAuthSignInPath = '/auth/signIn';
  static readonly getAuthHasLoginPath = '/auth/hasLogin';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Returns registration result
   * @param body Object for transfer sing up
   * @return Object for sing up
   */
  postAuthSignUpResponse(body?: SingUpDto): __Observable<__StrictHttpResponse<SignUpResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/auth/signUp`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SignUpResponse>;
      })
    );
  }
  /**
   * Returns registration result
   * @param body Object for transfer sing up
   * @return Object for sing up
   */
  postAuthSignUp(body?: SingUpDto): __Observable<SignUpResponse> {
    return this.postAuthSignUpResponse(body).pipe(
      __map(_r => _r.body as SignUpResponse)
    );
  }

  /**
   * Returns user info
   * @param body Object for transfer sing up
   * @return Object for sing in
   */
  postAuthSignInResponse(body?: SingInDto): __Observable<__StrictHttpResponse<SignInResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/auth/signIn`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SignInResponse>;
      })
    );
  }
  /**
   * Returns user info
   * @param body Object for transfer sing up
   * @return Object for sing in
   */
  postAuthSignIn(body?: SingInDto): __Observable<SignInResponse> {
    return this.postAuthSignInResponse(body).pipe(
      __map(_r => _r.body as SignInResponse)
    );
  }

  /**
   * Returns true is login use
   * @param login login for chech
   * @return Object for sing up
   */
  getAuthHasLoginResponse(login?: string): __Observable<__StrictHttpResponse<SignUpResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (login != null) __params = __params.set('login', login.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/auth/hasLogin`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SignUpResponse>;
      })
    );
  }
  /**
   * Returns true is login use
   * @param login login for chech
   * @return Object for sing up
   */
  getAuthHasLogin(login?: string): __Observable<SignUpResponse> {
    return this.getAuthHasLoginResponse(login).pipe(
      __map(_r => _r.body as SignUpResponse)
    );
  }
}

module AuthService {
}

export { AuthService }
