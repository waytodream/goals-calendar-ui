import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';
import { AppRotingModule } from './app-roting.module';
import { AppComponent } from './app.component';
import { UsersService } from './shared/services/users.service';
import { SystemModule } from './system/system.module';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    AppRotingModule,
    SystemModule,
    BrowserAnimationsModule,
  ],
  providers: [UsersService],
})
export class AppModule {}
