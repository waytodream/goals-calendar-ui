import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SystemComponent } from './system/system.component';

const routes: Routes = [
  { path: '', component: SystemComponent },
  {
    // tslint:disable-next-line:no-any
    loadChildren: (): Promise<any> => import('./auth/auth.module').then((m: any) => m.AuthModule),
    path: 'auth',
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})
export class AppRotingModule {}
