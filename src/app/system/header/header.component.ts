import { Component, OnInit, } from '@angular/core';
import { UserDto } from '../../api/models/user-dto';
import { UsersService } from '../../shared/services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public isLogged: boolean = false;
  public user: UserDto;

  constructor(private readonly usersService: UsersService) {
  }

  public ngOnInit(): void {
    this.user = this.usersService.getUser();
  }

  public exit(): void {
    this.usersService.logout();
  }
}
