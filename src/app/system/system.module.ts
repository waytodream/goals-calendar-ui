import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { CreationComponent } from './creation/creation.component';
import { GoalTreeComponent } from './goal-tree/goal-tree.component';
import { HeaderComponent } from './header/header.component';
import { MatrixComponent } from './matrix/matrix.component';
import { SystemRoutingModule } from './system-routing.module';
import { SystemComponent } from './system.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [SystemComponent, HeaderComponent, CalendarComponent, GoalTreeComponent, MatrixComponent, CreationComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    RouterModule,
    SystemRoutingModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule

  ],
  providers: [
    MatDatepickerModule,
  ],
})
export class SystemModule {
}
