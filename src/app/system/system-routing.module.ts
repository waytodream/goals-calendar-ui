import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { CreationComponent } from './creation/creation.component';
import { GoalTreeComponent } from './goal-tree/goal-tree.component';
import { MatrixComponent } from './matrix/matrix.component';
import { SystemComponent } from './system.component';

const routes: Routes = [
  {
    path: '', component: SystemComponent, children: [
      {path: 'calendar', component: CalendarComponent},
      {path: 'creation', component: CreationComponent},
      {path: 'goal-tree', component: GoalTreeComponent},
      {path: 'matrix', component: MatrixComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SystemRoutingModule {
}
