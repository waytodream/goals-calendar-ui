import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class BaseApi {
  private readonly baseUrl: string = 'http://192.168.0.102:8080/';

  constructor(public http: HttpClient) {}

  private getUrl(url: string = ''): string {
    return this.baseUrl + url;
  }

  public get<T>(url: string = ''): Observable<T> {
    return this.http.get<T>(this.getUrl(url));
  }

  public post<Q, T>(url: string = '', data: Q): Observable<T> {
    return this.http.post<T>(this.getUrl(url), data);
  }
}
