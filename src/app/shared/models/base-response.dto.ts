export class BaseResponseDto<T> {
  public code: number;
  public message: string;
  public data: T;
}
