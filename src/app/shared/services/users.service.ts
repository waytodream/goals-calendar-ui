import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as md5 from 'md5';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SingInDto } from '../../api/models/sing-in-dto';
import { UserDto } from '../../api/models/user-dto';
import { BaseApi } from '../core/base-api';
import { BaseResponseDto } from '../models/base-response.dto';
import { User } from '../models/user.model';

const PASSWORD_PREFIX: string = 'mystery';
const USER_LOCAL_STORAGE: string = 'user';

@Injectable()
export class UsersService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  public getUserByLogin(login: string): Observable<boolean> {
    return this.get<BaseResponseDto<boolean>>(`auth/hasLogin?login=${login}`).pipe(
      map<BaseResponseDto<boolean>, boolean>((response: BaseResponseDto<boolean>) => response.data),
    );
  }

  public createNewUser(user: User): Observable<boolean> {
    user.password = md5(PASSWORD_PREFIX + user.password);
    return this.post<User, BaseResponseDto<boolean>>(`auth/signup`, user).pipe(
      map<BaseResponseDto<boolean>, boolean>((response: BaseResponseDto<boolean>) => response.data),
    );
  }

  public singIn(user: SingInDto): Observable<BaseResponseDto<UserDto>> {
    user.password = md5(PASSWORD_PREFIX + user.password);
    return this.post<SingInDto, BaseResponseDto<UserDto>>(`auth/signIn`, user);
  }

  public setUser(user: UserDto): void {
    localStorage.setItem(USER_LOCAL_STORAGE, JSON.stringify(user));
  }

  public getUser(): UserDto {
    return JSON.parse(localStorage.getItem(USER_LOCAL_STORAGE));
  }

  public logout(): void {
    localStorage.removeItem(USER_LOCAL_STORAGE);
  }
}
