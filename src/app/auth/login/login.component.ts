import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { SingInDto } from '../../api/models/sing-in-dto';
import { UserDto } from '../../api/models/user-dto';
import { BaseResponseDto } from '../../shared/models/base-response.dto';
import { UsersService } from '../../shared/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  public loadingEnter: boolean = false;

  constructor(
    private readonly usersService: UsersService,
    private readonly router: Router,
    private readonly _snackBar: MatSnackBar
  ) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      login: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  public onSubmit(): void {
    this.loadingEnter = true;
    const user: SingInDto = this.form.value;
    this.usersService
      .singIn(user)
      .pipe(finalize(() => (this.loadingEnter = false)))
      .subscribe(
        (value: BaseResponseDto<UserDto>) => {
          this.usersService.setUser(value.data);
          this.router.navigate(['/']);
        },
        () => {
          this._snackBar.open('Ошибка входа', '', { duration: 2000 });
        }
      );
  }
}
