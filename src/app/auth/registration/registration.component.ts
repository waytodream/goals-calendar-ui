import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { User } from '../../shared/models/user.model';
import { UsersService } from '../../shared/services/users.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  public form: FormGroup;
  private readonly minLength: number = 6;
  public loadingLogin: boolean = false;
  public loadingSingUp: boolean = false;

  constructor(
    private readonly usersService: UsersService,
    private readonly router: Router,
    private readonly _snackBar: MatSnackBar
  ) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      login: new FormControl(null, [Validators.required], this.forbiddenLogin.bind(this)),
      password: new FormControl(null, [Validators.required, Validators.minLength(this.minLength)]),
      confirmation: new FormControl(null, [
        Validators.required,
        Validators.minLength(this.minLength),
        this.onConfirm,
      ]),
    });
  }

  public onSubmit(): void {
    this.loadingSingUp = true;
    const { name, login, password }: User = this.form.value;
    const user: User = new User(name, login, password);

    this.usersService.createNewUser(user)
      .pipe(finalize(() => this.loadingLogin = false))
      .subscribe(
      () => {
        this.router.navigate(['/auth/login']);
      },
      () => {
        this._snackBar.open('Ошибка регистрации', '', { duration: 2000, panelClass: 'error' });
      }
    );
  }

  public onConfirm(control: AbstractControl): Validators | null {
    if (control.value && control.parent && control.value === control.parent.get('password').value) {
      return null;
    } else {
      return { confirmationValidator: 'Пароли должны совпадать' };
    }
  }

  public forbiddenLogin(control: AbstractControl): Observable<ValidationErrors | null> {
    this.loadingLogin = true;
    return this.usersService.getUserByLogin(control.value).pipe(
      finalize(() => (this.loadingLogin = false)),
      map<boolean, ValidationErrors | null>((response: boolean) =>
        response ? { forbiddenLogin: 'Пользователь с таким login уже существует' } : null
      )
    );
  }
}
